module multiplexer(
	input [3:0] A, [1:0] sel,
	output logic Y
	);
	
	always_comb
	begin
		case (sel)
			0: Y = A[0]
			1: Y = A[1]
			2: Y = A[2]
			3: Y = A[3]
		endcase
	endcase
	
endmodule