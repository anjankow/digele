module lab1_prod(
	input [3:0] A,
	input B3, B2, B1, B0,
	output [7:0] C
	);
	reg [3:0] B [3:0];
	reg [3:0] AB [7:0];
	reg index = 0;
	reg [3:0] temp;

	always_comb
	begin
		
		// create an array of signals B0-B3	
		B[0] = {4{B0}};
		B[1] = {4{B1}};
		B[2] = {4{B2}};
		B[3] = {4{B3}};
		
		if (index < 4)
		begin
			// and gate
			temp = B[index] & A;
			// concatenate - unsigned bit extension
			AB[index] = {4'b0000, temp};
			// shift the bits
			AB[index] <<= index;
			
			index++;
		end
		
		// sum all the signals
		index = 0;
		
		if (index < 4)
		begin
			C += B[index];
			
			index++;
		end
		
	end
	
endmodule