`timescale 1ns / 1ps


/**
 * This module is a pulse counter with a parameterized width.
 * Careful: when the reset happens when the input signal is high,
 * when the reset goes low again, the signal is counted as one pulse.
 */
module pulsecnt #(
	parameter WIDTH = 8
	)(
    input i_sig,
    input i_rst,
    input i_clk,
    output [WIDTH-1:0] o_Pcnt
    );
    logic previous_sig;
    logic [WIDTH-1:0] cnt;
    
    assign o_Pcnt = cnt;
    
    always_ff@(posedge i_clk)
    begin
    	if (i_rst)
    	begin
    		cnt = 0;
    		previous_sig = 0;
    	end
    	else
    	begin
			if (i_sig && ~previous_sig)
			begin
				cnt += 1;
			end
			previous_sig = i_sig;
		end
    end
    
endmodule
