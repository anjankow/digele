`timescale 1ns / 1ps

/**
 * PWM generator
 *
 * The response for the input parameters is delayed by 1 clock signal.
 * If any of the input parameters change, the output is generated from the beginning.
 */
module pwmgenerator(
    input [15:0] i_duty,
    input [15:0] i_limit,
    input [7:0] i_n,
    input i_trig,
    input i_mode,
    input i_clk,
    input i_rst,
    output o_pwm
    );
    logic internal_reset;
    logic output_state;
    logic [15:0]duty_cnt;
    logic [15:0]limit_cnt;
    
    // to track the change of input signals 
    // and reset the internal signals if needed
    logic [15:0]previous_duty;
    logic [15:0]previous_limit;
    logic [15:0]previous_n;
    logic [15:0]previous_trig;
    logic [15:0]previous_mode;
        
    assign o_pwm = output_state;
            
    always_ff@(posedge i_clk)
    begin
    	if ((previous_duty != i_duty)
    		|| (previous_limit != i_limit)
    		|| (previous_n != i_n)
    		|| (previous_trig != i_trig)
    		|| (previous_mode != i_mode))
    	begin
    		internal_reset = 1;
    	end
    
    	if (i_rst || internal_reset)
    	begin
    		// reset all the internal signals
    		duty_cnt = 0;
			limit_cnt = 0;
			internal_reset = 0;
    	end
    	
    	if (limit_cnt < i_limit)
    	begin
    		limit_cnt += 1;
    	end
    	else
    	begin
    		// period is over
    		internal_reset = 1;
    	end
    	
    	output_state = 0;
		if (duty_cnt < i_duty)
		begin
			duty_cnt += 1;
			output_state = 1;
		end
    end

endmodule
