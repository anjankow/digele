`timescale 1ns / 1ps


/**
 * This module checks duty and period of a given PWM signal.
 * It checks only on the rising edge of the clock.
 * It means that if the signal is high for the moment much shorter than one period,
 * it does not matter - it is considered have duty=1.
 * Reset is synchronous and brings both outputs to low when it's high.
 * The output is updated only on next rising edge of the input signal (!).
 */ 
module dutyperiodmeas(
    input i_sig,
    input i_rst,
    input i_clk,
    output [15:0] o_duty,
    output [15:0] o_period
    );
    logic [15:0]duty;
    logic [15:0]period;
    logic [15:0]duty_reg;
    logic [15:0]period_reg;
    logic count_period;
    logic previous_sig;

	// output is assigned asynchronously from the registers
	assign o_duty = duty_reg;
	assign o_period = period_reg;
	
	always_ff@(posedge i_clk)
	begin
		if (i_rst)
		begin
			duty = 0;
			period = 0;
			count_period = 0;
			previous_sig = 0;
			duty_reg = 0;
			period_reg = 0;
		end

		else
		begin		
			if (i_sig)
			begin
				count_period = 1;
	
				if (previous_sig)
				begin
					duty += 1;
					period += 1;
				end
				else
				begin
					duty_reg = duty;
					period_reg = period;
					// start counting from one
					duty = 1;
					period = 1;
				end
	
			end
			else
				// this if is in case if reset occurred and 
				// input singal was not yet high
				if (count_period)
					period += 1;
			
			previous_sig = i_sig;
		end
	end
	
endmodule