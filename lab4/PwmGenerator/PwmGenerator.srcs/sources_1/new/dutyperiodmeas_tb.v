`timescale 1ns / 1ps

import testhelper::*;
      
module dutyperiodmeas_tb(

    );
    
    logic i_sig = 0;
    logic i_rst = 1;
    logic i_clk = 1;
    logic [15:0] o_duty;
    logic [15:0] o_period;
    
    
    always #`CLK_HALF_PERIOD i_clk = ~i_clk;
    
    dutyperiodmeas DUT(.*);
    
    initial
    begin
    	int clk_period = `CLK_HALF_PERIOD * 2;
    	#clk_period;
    	i_rst = 0;
    	#(3*clk_period);
    	// signal goes high - start counting
    	i_sig = 1;
    	#(8*clk_period);
    	// signal goes low - duty = 8
    	i_sig = 0;
    	#(5*clk_period);
    	// signal goes high again - output shall be assigned
    	i_sig = 1;
    	#clk_period; // to give some time to update the output
    	`assert(8, o_duty);
    	`assert(13, o_period);

    	// now check the boundary conditions
    	// duty = 1, period = 2
    	// smallest period = 2, because it needs one clock cycle to realize
    	// that signal is low and another one to realize that next 
    	// signal rising edge occurred
    	i_sig = 0;
    	#(clk_period);
    	i_sig = 1;
    	#(clk_period);
    	`assert(1, o_duty);
    	`assert(2, o_period);

    	// duty = 1, period = 6
    	i_sig = 0;
    	#(clk_period*6);
    	i_sig = 1;
    	#(clk_period/2); // to give some time to update the output
    	`assert(1, o_duty);
    	`assert(7, o_period);

		// checking reset when signal is high
		#(clk_period*4);
		i_rst = 1;
		#(clk_period);
    	`assert(0, o_duty);
    	`assert(0, o_period);
    	i_rst = 0;
    	// signal is high all the time, reset goes low
		#(clk_period*2);
		i_sig = 0;
		#(clk_period);
		i_sig = 1;
    	#(clk_period); // to give some time to update the output
    	`assert(2, o_duty);
    	`assert(3, o_period);

    	// checking the reset when the signal is low
    	i_sig = 0;
    	#(clk_period*4);
    	i_rst = 1;
    	#(clk_period);
    	i_rst = 0;
    	#(clk_period*6);
    	i_sig = 1;
    	#(clk_period); // to give some time to update the output
    	`assert(0, o_duty);
    	`assert(0, o_period);

    	$finish;
    end
endmodule