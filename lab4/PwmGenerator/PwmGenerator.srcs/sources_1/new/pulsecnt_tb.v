`timescale 1ns / 1ps

import testhelper::*;

module pulsecnt_tb(

    );
	logic i_sig = 0;
    logic i_rst = 1;
    logic i_clk = 1;
    logic [7:0] o_Pcnt;

	// will be tested with 8-bit width
	pulsecnt #(.WIDTH(8)) DUT (.*);
	
	always #`CLK_HALF_PERIOD i_clk = ~i_clk;
	
	initial
	begin
		int clk_period = `CLK_HALF_PERIOD * 2;
    	#clk_period;
    	i_rst = 0;
    	#(3*clk_period);
    	
    	// generate few pulses with a different width
    	// each time check the counter value
    	i_sig = 1;
    	#(clk_period);
    	`assert(1, o_Pcnt);
    	i_sig = 0;
    	#(clk_period);
    	i_sig = 1;
    	#(4*clk_period);
    	`assert(2, o_Pcnt);
    	i_sig = 0;
    	#(clk_period);
    	i_sig = 1;
    	#(clk_period*4);
    	`assert(3, o_Pcnt);
    	
    	// reset called when signal is high
    	i_rst = 1;
    	#(clk_period);
    	`assert(0, o_Pcnt);
    	i_rst = 0;
    	#(clk_period);
    	// since signal was high, this shall count as a pulse
    	`assert(1, o_Pcnt);
    	i_sig = 0;
    	#(clk_period*4);
    	
    	// reset called when the signal is low
    	i_rst = 1;
    	#(clk_period);
    	`assert(0, o_Pcnt);
    	i_rst = 0;
    	#(clk_period*4);
    	`assert(0, o_Pcnt);
    	// no pulse happened, assert 0
    	i_sig = 1;
    	#(clk_period);
    	`assert(1, o_Pcnt);
    	$finish;
	end
	

endmodule
