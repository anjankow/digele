`timescale 1ns / 1ps

import testhelper::*;
      
module pwmgenerator_tb(

    );
    
    logic [15:0] i_duty = 0;
    logic [15:0] i_limit = 0;
    logic [7:0] i_n = 0;
    logic i_trig = 0;
    logic i_mode = 0;
    logic i_clk = 1;
    logic i_rst = 1;
    logic o_pwm;
    
    
    always #`CLK_HALF_PERIOD i_clk = ~i_clk;
    
    pwmgenerator DUT(.*);
    
    initial
    begin
    	int clk_period = `CLK_HALF_PERIOD * 2;
    	#`CLK_HALF_PERIOD;
    	#clk_period;
    	i_rst = 0;
    	#clk_period;
    	
    	i_duty = 3;
    	i_limit = 5;
    	// output shall go high with the next clock cycle
    	`assert(0, o_pwm);
    	#clk_period;
    	`assert(1, o_pwm);
    	#clk_period;
    	// after 2 periods output shall still be high
    	#(2*clk_period);
    	`assert(1, o_pwm);
    	// and go low on the third cycle
    	#(clk_period);
    	`assert(0, o_pwm);
    	// shall stay low for next 2 cycles
    	 #(clk_period*2);
    	`assert(0, o_pwm);
    	// and go high again
    	 #(clk_period);
    	`assert(1, o_pwm);
    	
    	
    	$finish;
    end
endmodule