package testhelper;
	
	`define assert(expected, given) \
        if (expected != given) begin \
            $display("ASSERTION FAILED in %m: expected = %0d, received = %0d.", expected, given); \
        end
        
	`define CLK_HALF_PERIOD 1ns

endpackage