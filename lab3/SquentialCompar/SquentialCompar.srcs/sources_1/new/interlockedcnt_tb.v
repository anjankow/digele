`timescale 1ns / 1ps

`define CLOCK 5ns
`define CLK_PERIOD (`CLOCK * 2)

module interlockedcnt_tb();

	logic clk = 0, in_x = 0;
	logic [2:0]out_y;
	
	always #`CLOCK clk = ~clk;
	
	interlockedcnt DUT(
		.x(in_x),
		.clk(clk),
		.y(out_y));
	
	initial
	begin
		// wait 2ns, so rising clock edge and
		// signals change will not overlap
		#2ns
		// x is not set to 0, which means that for the first 
		// 2 periods the output shall be undefined
		#(`CLK_PERIOD * 2);
		in_x <= 1;
		// now x is high - output of the down counter shall 
		// be set to 4 and up counter shall reset
		#(`CLK_PERIOD * 20);
		in_x <= 0; 
		// x is set to low - counters can start counting
		#(`CLK_PERIOD * 2);
		$dumpvars(1, interlockedcnt);
		#(`CLK_PERIOD * 60);
		in_x <= 1;
		// output shall be 4 now
		#`CLK_PERIOD
		in_x <= 0;
		// and here the counter should start counting
		#(`CLK_PERIOD * 20);
		in_x <= 1;
		// output shall be 4 again
		#`CLK_PERIOD;
		in_x <= 0;
		// and counting
		#(`CLK_PERIOD * 40);
		$finish;
	end


endmodule
