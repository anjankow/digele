`timescale 1ns / 1ps


module interlockedcnt(
    input x,
    input clk,
    output logic [2:0] y
    );

    // c_value_down is a output value of the down counter
    logic [2:0] c_value_down;
    // c_value_up is a output value of the up counter
    logic [3:0] c_value_up;
    
    // cmp_down_cnt is a result of comparing
    // the output of down counter with 0;
    // if the comparator output is bigger than 0,
    // this variable is set to 1 
    logic cmp_down_cnt;
    
    // cmp_up_cnt is a result of comparing
    // the output of up counter with 10;
    // if the comparator output is equal 10,
    // this variable is set to 1
    logic cmp_up_cnt;
    
    // output is equal to the down counter output  
    assign y = c_value_down;
    
	// =========================================== //

	// This is a functionality of the down counter.
	// When set2four is 1, the output shall be 4.
	// This is master input. 
	// Else, is enable input is 1, counter should count down.
    always_ff @(posedge clk)
    begin
    	// x is connected to set2four input
    	if (x)
    		c_value_down <= 3'b100;
    	else
    		if (cmp_up_cnt)
    			c_value_down <= c_value_down - 1;
    		// else counter shall keep its value
    end
    
    // This is to compare output
    // of the down counter with 0. 
    always_comb
    begin
    	if (c_value_down > 3'b0)
    		cmp_down_cnt = 1;
    	else
    		cmp_down_cnt = 0;
    end
    
    // This is a functionality of the up counter.
    // If reset input is set to 1, the counter output is 0.
    // Otherwise, is enable is 1, the counter increases
    // the output value by 1 with every posedge of the clock.
    always_ff @(posedge clk)
    begin
    	// if any of signals x or cmp_up_cnt is high,
    	// the output of this comparator is set to 0
    	if (x | cmp_up_cnt)
    		c_value_up = 0;
    	else
    		if (cmp_down_cnt)
    			c_value_up <= c_value_up + 1;
    		// else counter shall keep its value
    end
    
    // This is to compare output
    // of the up counter with 10.
    always_comb
    begin
    	if (c_value_up == 4'b1010)
    		cmp_up_cnt = 1;
    	else
    		cmp_up_cnt = 0;
    end
    
endmodule
