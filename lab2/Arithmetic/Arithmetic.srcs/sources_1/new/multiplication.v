`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/26/2020 02:43:12 AM
// Design Name: 
// Module Name: multiplication
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiplication(
    input [3:0] A,
    input B3,
    input B2,
    input B1,
    input B0,
    output [7:0] C
    );
    reg [7:0] ab0;
    reg [7:0] ab1;
    reg [7:0] ab2;
    reg [7:0] ab3;
    reg [7:0] outputReg;
    
    // The function performs one 'branch' of the multiplication operation
    // - and gate and shifting. Parameter bitNum informs how many byts should be shifted.
    function [7:0] multOperationPart;
        input [3:0] A;
        input B;
        input int bitNum; // how many bits should be shifted
        logic [3:0] bReplicated; // B replicated on 4 bits
        logic [3:0] andGateResult; // result of A and bReplicated
        
        bReplicated = {4{B}};
        andGateResult = A & bReplicated;
        multOperationPart = {4'b0, andGateResult} << bitNum;
        
    endfunction
    
    
    always_comb
    
    begin
        // execute multOperationPart function on each B bit on the input
        ab0 = multOperationPart(A, B0, 0);
        ab1 = multOperationPart(A, B1, 1);
        ab2 = multOperationPart(A, B2, 2);
        ab3 = multOperationPart(A, B3, 3);
        // output is a sum of these par
        outputReg = ab0 + ab1 + ab2 + ab3;
    end
    
    // assign output to the output register changed in the always_comb block
    assign C = outputReg;
    
endmodule
