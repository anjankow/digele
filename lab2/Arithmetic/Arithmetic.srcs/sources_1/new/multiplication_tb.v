`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/26/2020 02:43:11 AM
// Design Name: 
// Module Name: multiplication_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiplication_tb();
	
	// inputs and output of the DUT
	logic [3:0] A;
	logic [3:0] B;
	logic [7:0] C;
	
	// declare DUT
	multiplication DUT(
		.A(A),
		.B3(B[3]),
		.B2(B[2]),
		.B1(B[1]),
		.B0(B[0]),
		.C(C));
		
	initial
	begin
		// first set both A and B to 0 -> the result should be 0
		A = 4'b0;
		B = 4'b0;
		# 10ns;
		$display("Expected 0, received: %d", C);
		
		// now test multiplication by 0 -> the result should be 0
		A = 4'b0;
		B = 4'b1111;
		# 10ns;
		$display("Expected 0, received: %d", C);
		A = 4'b1111;
		B = 4'b0;
		# 10ns;
		$display("Expected 0, received: %d", C);
		
		
		// now let's test some arbitary input values
		A = 4'b1010; // 10
		B = 4'b1001; // 9
		#10ns;
		$display("Expected 90, received: %d", C);
		
		A = 4'b1000; // 8
		B = 4'b0111; // 7
		#10ns;
		$display("Expected 56, received: %d", C);
		
		A = 4'b0011; // 3
		B = 4'b0111; // 7
		#10ns;
		$display("Expected 21, received: %d", C);
		
		
		// finally let's check the boundary values
		A = 4'b1111; // 15
		B = 4'b1111; // 15
		#10ns;
		$display("Expected 225, received: %d", C);
				
		// in addition test negative numbers
		A = 4'b1110; // -2 signed, 14 unsigned
		B = 4'b0100; // 4 unsigned
		#10ns;
		$display("Expected signed: 11111000 (-8), received: %b", C);
		
		$finish;
	end
endmodule
