`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.10.2020 14:44:59
// Design Name: 
// Module Name: unicomp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module unicomp(
    input a,
    input b,
    input output_en, //when inactive, outputs are LOW
    output logic A_lt_B, // A smaller than B
    output logic A_eq_B, // A equal to B
    output logic A_bt_B  // A bigger than B
    );
    
    always_comb
    begin
        if(output_en)
        begin
            A_lt_B = (~a) & b;
            A_bt_B =a & (~b);
            A_eq_B = ~( A_lt_B | A_bt_B);
        end
        else
        begin
            A_lt_B = 1'b0;
            A_bt_B = 1'b0;
            A_eq_B = 1'b0;
        end
    end
endmodule
