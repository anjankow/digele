`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.10.2020 14:44:59
// Design Name: 
// Module Name: unicomp_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module unicomp_tb();

    logic enbl;
    logic [1:0] ab;
    logic A_lt_B, A_bt_B, A_eq_B;
    integer i = 0;
    
    // instantiate DUT - unicomp 
    unicomp DUT(
        .a(ab[1]),
        .b(ab[0]),
        .output_en(enbl),
        .A_lt_B(A_lt_B),
        .A_bt_B(A_bt_B),
        .A_eq_B(A_eq_B));
      
    initial
    begin
    
        // set enable to LOW
        enbl = 0;

        // using the loop, test all the combinations of signals ab and enbl
        while (i < 2)
        begin
            // change enable to HIGH on the first loop run
            // and then again to LOW on the next loop run
            enbl += 1;
            
            ab = 0;
            #10ns; // both a and b are equal to 0
            ab += 1;
            #10ns; // b equals 1 and a is 0 
            ab += 1;
            #10ns; // a equals 1 and b is 0
            ab += 1;
            #10ns; // both a and b are equal 1
            
            i += 1;
        end

        $finish;
    end
endmodule
